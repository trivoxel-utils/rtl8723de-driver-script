#!/bin/bash
# Script written by TriVoxel (https://TriVoxel.page.link/site)
# Code taken from https://askubuntu.com/questions/983251
# Feel free to modify any part of this script and re-distribute it.

## Kill program on any errors
set -e

## If this isn't being run as root, request it
if [ $(id -u) -ne 0 ];then
	echo "Please run as root. Exiting.";sleep 2;exit
fi

## Full Offline Mode
if [ $1 = '--offline' ];then
	echo "Offline mode enabled."
	offline=1
## I don't have time to fix my custom path solution right now
#	if [ -d $2 ];then
#		path=$2
#	else
#		echo "No valid path set. Exiting."
#		exit 1
#	fi
else
	echo "Online mode enabled."
	offline=0
        path=/tmp/rtl_wifi
fi

## Notify user they need WiFi
if [ $offline = '1' ];then
	echo -e "\nRunning in offline mode..."
else
	echo -e "\nThis program requires an internet connection to complete. If you have a wired\nconnection, use that. Otherwise, if you have another Linux device, you can\nuse the information from https://askubuntu.com/questions/359856/ to share your\nother device's WiFi via Ethernet. If this needs to be offline, download the\ncontents from https://github.com/lwfinger/rtlwifi_new.git and save it to\n'/tmp/rtlwifi_new' from another device. You can run this script with the\n'--offline' flag and point to the path you saved it to.\n"
fi

## Make it easier for the user to understand which platform they use and I support
echo -e "Currently supported platforms include:\n\nDebian Derivatives:\n  >Deepin\n  >Linux Mint\n  >Ubuntu\n  >ZorinOS\nArch Linux Derivatives:\n  >Antergos\n  >Manjaro\n  >RebornOS\n"

## This needs to be skipped for anyone offline. Else it will error out due to 404
if [ $offline = '0' ];then
	## This will just check the operating system. More distributions are planned for future releases.
	echo "Which distribution do you use? (type 'debian' or 'arch')"
	read distro
	if [ $distro = "debian" ]; then
		echo "Installing dependencies via Apt..."
		sudo apt-get install build-essential linux-headers-$(uname -r) git dkms
	elif [ $distro = "arch" ];then
		echo "Installing dependencies via Pacman..."
		sudo pacman -S linux-headers git dkms
	else
		echo "No valid distribution specified. Exiting."
		exit 1
	fi
	## Fetch the current source code from online
        git clone -b extended --single-branch https://github.com/lwfinger/rtlwifi_new.git $path
fi

## Where the magic happens!
echo "Installing modules. Please wait..."
sleep 1
#cd /tmp/
sudo dkms add $path
#if [ $3 = "--reinstall" ];then
#	sudo dkms remove rtlwifi-new/0.6 --all
#fi
sudo dkms install rtlwifi-new/0.6
echo "Done.";sleep 1

## Prompts the user to set their antenna. Most vendors use antenna 2 for the official Windows driver.
echo -e "Which antenna would you like to use? (Default 2)\nType a value from 1-3 then hit enter:"
read ant
if [[ '1 2 3' == *$ant* && -z $ant ]];then
	sudo tee /etc/modprobe.d/rtl8723de.conf <<< "options rtl8723de ant_sel=$ant"
else
	sudo tee /etc/modprobe.d/rtl8723de.conf <<< "options rtl8723de ant_sel=2"
	ant=2
fi
echo "Using antenna $ant."

## Driver requires a reboot to fully activate
echo "All operations complete. Reboot? (y/n)"
read poll
if [ $poll = y ];then
	echo "Computer will reboot automatically in five (5) seconds..."
	sleep 5
	reboot
else;echo "Reboot cancelled. WiFi should work on next boot! Exiting."
exit
