#!/bin/bash
# Script written by TriVoxel (https://TriVoxel.page.link/site)
# Code taken from https://askubuntu.com/questions/983251 and https://h30434.www3.hp.com/t5/Notebook-Wireless-and-Networking/a/td-p/6477307
# Report any bugs to https://gitlab.com/TriVoxel/rtl8723de-driver-script/issues
# Feel free to modify any part of this script and re-distribute it.

## Kill program on any errors
set -e

## If this isn't being run as root, request it
if [ $(id -u) -ne 0 ];then
	echo "Please run as root. Exiting.";sleep 1;exit
fi

## Full Offline Mode
if [[ $1 = '--offline' ]];then
	echo "Offline mode enabled."
	offline=1
## I still don't have time to implement custom user-set paths yet. This is an arbitrary, unimplemented hack.
#	if [ -d $2 ];then
#		wPath=$2
#	else
#		echo -e No valid path set.\nUsage: <this_script.sh> /path/to/wifi/source /path/to/bluetooth/source\nExiting.
#		exit
#	fi
#	if [ -d $3 ];then
#		bPath=$3
#	else
#		echo -e No valid path set.\nUsage: <this_script.sh> /path/to/wifi/source /path/to/bluetooth/source\nExiting.
#		exit
#	fi
else
	echo "Online mode enabled."
	offline=0
	wPath=/tmp/rtl_wifi
	bPath=/tmp/rtl_bt
fi

echo -e "Please report any bugs to https://gitlab.com/TriVoxel/rtl8723de-driver-script/issues/\n"

if [ $offline = 0 ];then
	echo "Install dependencies? [Y/n]"
	read depPoll
	if [ $depPoll = Y ];then
		## This will just check the operating system. More distributions are planned for future releases.
                ## Make it easier for the user to understand which platform they use and I support
                echo -e "Currently supported platforms include:\n\nDebian Derivatives:\n  >Deepin\n  >Linux Mint\n  >Ubuntu\n  >ZorinOS\nArch Linux Derivatives:\n  >Antergos\n  >Manjaro\n  >RebornOS\n"
                echo "Which distribution do you use? (type 'debian' or 'arch')"
                read distPoll
                if [ $distPoll = debian ]; then
                        echo "Installing dependencies via Apt..."
			sudo apt-get update -y
                        sudo apt-get install build-essential linux-headers-$(uname -r) git dkms
                elif [ $distPoll = arch ];then
                        echo "Installing dependencies via Pacman..."
                        sudo pacman -Syy --noconfirm linux-headers git dkms
                else
                        echo "No valid distribution specified. Exiting."
                        exit
                fi
	else
		echo "Skipping dependency install..."
		sleep .5
	fi
fi

echo "Install WiFi modules? [Y/n]"
read wPoll
if [ $wPoll = Y ];then
	## Notify user they need WiFi
	if [ $offline = 1 ];then
                echo -e "\nRunning in offline mode..."
	else
                echo -e "\nThis program requires an internet connection to complete. If you have a wired\nconnection, use that. Otherwise, if you have another Linux device, you can\nuse the information from https://askubuntu.com/questions/359856/ to share your\nother device's WiFi via Ethernet. If this needs to be offline, download the\ncontents from https://github.com/lwfinger/rtlwifi_new.git and save it to\n'/tmp/rtlwifi_new' from another device. You can run this script with the\n'--offline' flag and point to the path you saved it to.\n"
	fi

	## This needs to be skipped for anyone offline. Else it will error out due to 404
	if [ $offline = 0 ];then
                if [ ! -d $wPath ];then
                    ## Fetch the current WiFi source code from online
                    echo "Cloning WiFi modules..."
                    sleep .5
                    git clone -b extended --single-branch https://github.com/lwfinger/rtlwifi_new.git $wPath
                fi
	fi

	## Where the magic happens!
	echo "Installing modules. Please wait..."
	sleep 1
	cd /tmp/
#	ls # This was for me while debugging to make sure it was reading the right path
	sleep 1
	if [ $2 = --reinstall ];then ## This is really a hacky method, but it works for now.
		echo ">>>dkms remove rtlwifi-new/0.6 --all"
		sudo dkms remove rtlwifi-new/0.6 --all
	fi
	echo "==> sudo dkms add ..."
	sudo dkms add rtl_wifi
#	echo "==> sudo dkms build ..."
#	sudo dkms build rtlwifi-new/0.6 -k $(uname -r) --force
	echo "==> sudo dkms install .../0.6"
	sudo dkms install rtlwifi-new/0.6 -k $(uname -r) --force
	echo "Done.";sleep 1

	## Prompts the user to set their antenna. Most vendors use antenna 2 for the official Windows(R) driver.
        echo -e "Which antenna would you like to use? (Default 2)\nType a value from 1-3 then hit enter:"
	read ant
	if [[ "1 2 3" == *$ant* && -z $ant ]];then
		sudo tee /etc/modprobe.d/rtl8723de.conf <<< "options rtl8723de ant_sel=$ant"
	else
		sudo tee /etc/modprobe.d/rtl8723de.conf <<< "options rtl8723de ant_sel=2"
		ant=2
	fi
	echo "Using antenna $ant."
else
    echo "Skipping WiFi install..."
    sleep .5
fi

## Bluetooth Support
echo "Install Bluetooth modules? [Y/n]"
read bPoll
if [ $bPoll = Y ];then
	if [ $offline = 0 ];then
                if [ ! -d $bPath ];then
                    ## Fetch current Bluetooth source code from online
                    echo "Cloning Bluetooth modules..."
                    sleep .5
                    git clone -b master --single-branch https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git $bPath
                fi
	fi
	## Create directories
        if [ ! -d /lib/firmware/ ];then
		mkdir -v /lib/firmware/
	fi
        if [ ! -d /lib/firmware/rtl_bt/ ];then
                mkdir -v /lib/firmware/rtl_bt/
	fi
	## Copy bluetooth modules
	echo "Installing Bluetooth modules..."
	sleep .5
	cp -v $bPath/rtl_bt/rtl8723d_fw.bin /lib/firmware/rtl_bt/
	cp -v $bPath/rtl_bt/rtl8723d_config.bin /lib/firmware/rtl_bt/
        cp -v $bPath/rtlwifi/rtl8723defw.bin /lib/firmware/rtl_bt/
	echo "Bluetooth successfully installed."
else
    echo "Skipping Bluetooth install..."
    sleep .5
fi

## Driver requires a reboot to fully activate
echo "All operations complete. Reboot? [Y/n]"
read rebPoll
if [ $rebPoll = Y ];then
	echo -e "Computer will reboot automatically in three (3) seconds...\nPress [Ctrl+C] to cancel."
	sleep 3
	reboot
else
	echo "Reboot cancelled. WiFi should work on next boot! Exiting..."
	exit
fi
